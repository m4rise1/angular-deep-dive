import {Request, Response} from 'express';
import {COURSES} from '../src/db-data';


export function getAllCourses(req: Request, res: Response) {
    let value = COURSES;
    const {page, pageSize} = req.query;
    if (page && pageSize) {
        value = value.slice(((page-1)*pageSize), (page*pageSize));
    }

    // res.status(200).json({payload: Object.values(COURSES)});
    res.status(200).json(value);
}


export function getCourseById(req: Request, res: Response) {
    const courseId = req.params['id'];
    const courses: any = Object.values(COURSES);
    const course = courses.find(course => course.id == courseId);

    res.status(200).json(course);
}