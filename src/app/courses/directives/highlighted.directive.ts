import { Directive, EventEmitter, HostBinding, HostListener, Input, Output } from '@angular/core';

@Directive({
    selector: '[highlighted]',
    exportAs: 'hl',
})
export class HighlightedDirective {
    // Même nom que la directive mais peut être différent
    @Input('highlighted')
    isHighlighted = false;

    @Output()
    toggleHighlighted = new EventEmitter();

    // Modif d'une propriété du DOM...
    @HostBinding('class.highlighted')
    get getClassName() {
        return this.isHighlighted;
    }
    // ... ou d'un attribut HTML
    @HostBinding('attr.disabled')
    get disabled() {
        return true;
    }

    // Bind évènement
    @HostListener('mouseenter', ['$event'])
    onMouseEnter($event) {
        //console.log($event);
        this.isHighlighted = true;
        this.toggleHighlighted.emit(this.isHighlighted);
    }

    @HostListener('mouseleave')
    onMouseLeave() {
        this.isHighlighted = false;
        this.toggleHighlighted.emit(this.isHighlighted);
    }

    toggle() {
        return (this.isHighlighted = !this.isHighlighted);
    }

    constructor() {
        //console.log('directive highlited');
    }
}
