import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';

/**
 * Création d'une directive structurelle *ngUnless
 * sur le modèle simplifié de *ngIf
 */
@Directive({
    selector: '[ngxUnless]',
})
export class NgxUnlessDirective {
    /**
     * Eviter de recréer la vue ou de clear inutilement
     */
    visible = false;

    constructor(private templateRef: TemplateRef<any>, private viewContainer: ViewContainerRef) {}

    @Input()
    set ngxUnless(condition: boolean) {
        if (!condition && !this.visible) {
            this.viewContainer.createEmbeddedView(this.templateRef);
            this.visible = true;
        } else if (condition && this.visible) {
            this.viewContainer.clear();
            this.visible = false;
        }
    }
}
