import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CourseCardComponent } from './course-card/course-card.component';
import { CardImageComponent } from './card-image/card-image.component';
import { CardCustomTextComponent } from './card-custom-text/card-custom-text.component';
import { HighlightedDirective } from './directives/highlighted.directive';
import { NgxUnlessDirective } from './directives/ngx-unless.directive';
import { CoursesService } from '../services/courses.service';
import { FilterByCategoryPipe } from './pipes/filter-by-category.pipe';

@NgModule({
    declarations: [
        CourseCardComponent,
        CardCustomTextComponent,
        CardImageComponent,
        HighlightedDirective,
        NgxUnlessDirective,
        FilterByCategoryPipe,
    ],
    imports: [CommonModule],
    providers: [CoursesService],
    exports: [
        CourseCardComponent,
        CardImageComponent,
        CardCustomTextComponent,
        HighlightedDirective,
        NgxUnlessDirective,
        FilterByCategoryPipe,
    ],
})
export class CoursesModule {}
