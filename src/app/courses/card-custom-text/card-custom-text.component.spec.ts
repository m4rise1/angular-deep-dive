import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardCustomTextComponent } from './card-custom-text.component';

describe('CardCustomTextComponent', () => {
    let component: CardCustomTextComponent;
    let fixture: ComponentFixture<CardCustomTextComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [CardCustomTextComponent],
        }).compileComponents();

        fixture = TestBed.createComponent(CardCustomTextComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
