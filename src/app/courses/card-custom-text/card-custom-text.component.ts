import { Component, Input } from '@angular/core';

@Component({
    selector: 'card-custom-text',
    templateUrl: './card-custom-text.component.html',
    styleUrls: ['./card-custom-text.component.css'],
})
export class CardCustomTextComponent {
    @Input('cstmtext')
    customText: string;
}
