import {
    AfterContentInit,
    Component,
    ContentChild,
    ContentChildren,
    ElementRef,
    EventEmitter,
    Input,
    Output,
    TemplateRef,
} from '@angular/core';
import { CardCustomTextComponent } from '../card-custom-text/card-custom-text.component';
import { Course } from '../../model/course';

@Component({
    selector: 'course-card',
    templateUrl: './course-card.component.html',
    styleUrls: ['./course-card.component.css'],
})
export class CourseCardComponent implements AfterContentInit {
    @Input()
    course: Course;

    @Input()
    cardIndex: number;

    @Input()
    noImageTpl: TemplateRef<any>;

    @Output('courseClicked')
    imgclicked = new EventEmitter<Course>();

    @Output('courseChanged')
    courseChanged = new EventEmitter<Course>();

    // Pareil à ViewChild() mais pour du contenu projeté dans <ng-content>
    @ContentChild(CardCustomTextComponent)
    customTextComp: CardCustomTextComponent;

    ngAfterContentInit(): void {
        //console.log(this.customTextComp);
    }

    onTitleChanged(newTitle: string) {
        this.course.description = newTitle;
    }

    onSaveClicked(newTitle: string) {
        this.courseChanged.emit({ ...this.course, description: newTitle });
    }

    courseClicked() {
        this.imgclicked.emit(this.course);
    }

    isImageUrl(): boolean {
        return this.course?.iconUrl !== undefined;
    }

    cardClasses() {
        /**
         * return possibles :
         * 'testClass' || ['test1', 'test2'] || {"test1": true, "test2": false}
         */
        return {
            beginner: this.course.category === 'BEGINNER',
        };
    }

    cardStyle() {
        if (this.isImageUrl() && this.cardIndex === 9) {
            return {
                'background-image': 'url(' + this.course.iconUrl + ')',
            };
        }
    }
}
