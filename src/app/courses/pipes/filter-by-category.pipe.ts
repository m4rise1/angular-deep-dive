import { Pipe, PipeTransform } from '@angular/core';
import { Observable } from 'rxjs';
import { Course } from 'src/app/model/course';

@Pipe({
    name: 'filterByCategory',
})
export class FilterByCategoryPipe implements PipeTransform {
    names = {
        BEGINNER: 'Padawan',
        INTERMEDIATE: 'Jedi Knight',
        ADVANCED: 'Jedi Master',
    };

    transform(course: Course): Course {
        course.category = this.names[course.category] ?? course.category;
        return course;
    }
}
