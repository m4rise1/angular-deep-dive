import {
    AfterViewInit,
    Component,
    ElementRef,
    Inject,
    OnInit,
    QueryList,
    ViewChild,
    ViewChildren,
} from '@angular/core';

import { CourseCardComponent } from './courses/course-card/course-card.component';
import { HighlightedDirective } from './courses/directives/highlighted.directive';
import { Course } from './model/course';
import { Observable } from 'rxjs';
import { CoursesService } from './services/courses.service';
import { AppConfig, APP_CONFIG, CONFIG_TOKEN } from './config';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
    /**
     * Utilisation d'un provider: attention sera toujours fourni même si pas utilisé
     * Voir config.ts pour une solution tree-shakable
     */
    /*
  providers: [
    {provide: CONFIG_TOKEN, useFactory: () => APP_CONFIG}
  ]
  */
})
export class AppComponent implements AfterViewInit, OnInit {
    courses$: Observable<Course[]>;
    startDate = new Date(1992, 0, 30);

    /**
     * Query la vue LOCALE
     * en param: une variable référencée dans template ou UN composant, read property optionnelle
     * en type: ElementRef ou Composant (si composant en param)
     */
    @ViewChild('cardRef', { read: ElementRef })
    card: ElementRef;

    // Idem ViewChild mais pour des collections
    @ViewChildren(CourseCardComponent)
    cards: QueryList<CourseCardComponent>;

    // Récupération de la directive depuis le composant
    @ViewChildren(CourseCardComponent, { read: HighlightedDirective })
    // Alternative :récupération de la directive directement
    // @ViewChildren(CourseCardComponent)
    highlighted: QueryList<HighlightedDirective>;

    constructor(
        private coursesService: CoursesService,
        @Inject(CONFIG_TOKEN) private config: AppConfig // Dans le cas d'une injection d'un objet
    ) {}

    ngOnInit(): void {
        console.log(this.config);
        this.courses$ = this.coursesService.getAll();
    }

    onCourseClicked(course: Course) {
        // console.table(course);
    }

    onCourseChanged(course: Course) {
        this.coursesService.saveCourse(course).subscribe((res) => console.log(res.description));
    }

    onToggleHighlighted(isHighlighted: boolean, index: number) {
        // console.log(`card index ${index} highlighted ${isHighlighted}`);
    }

    ngAfterViewInit(): void {
        // console.log('card', this.card);
        // console.log('cards', this.cards);
        // console.log(this.highlighted);
    }
}
